import logging
import os

import azure.functions as func

from azure.common.credentials import get_azure_cli_credentials
from msrestazure.azure_active_directory import MSIAuthentication

# import resource from common.py
from __app__.shared.common import list_nsgs


async def main(req: func.HttpRequest) -> func.HttpResponse:
    """
    The main entry point to the function.
    """

    if "MSI_ENDPOINT" in os.environ:
        credentials = MSIAuthentication()
    else:
        credentials, *_ = get_azure_cli_credentials()

    subscription_id = os.environ.get('AZURE_SUBSCRIPTION_ID', None)
    if subscription_id:
        list_of_nsgs = await list_nsgs(credentials, subscription_id, "myResourceGroup")
        return func.HttpResponse(list_of_nsgs, mimetype="application/json")
    else:
        return func.HttpResponse(
             "Please configure AZURE_SUBSCRIPTION_ID variable with subscription ID value",
             status_code=400
        )