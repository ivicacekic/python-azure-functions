import logging
import json

from azure.mgmt.resource import ResourceManagementClient
from azure.mgmt.network import NetworkManagementClient


def process_rg_instance(group):
    """
    Get the relevant pieces of information from a ResourceGroup instance.
    """
    return {
        "Name": group.name,
        "Id": group.id,
        "Location": group.location,
        "Tags": group.tags,
        "Properties": group.properties.provisioning_state if group.properties and group.properties.provisioning_state else None
    }


async def list_rgs(credentials, subscription_id):
    """
    Get list of resource groups for the subscription id passed.
    """
    list_of_resource_groups = []

    with ResourceManagementClient(credentials, subscription_id) as rg_client:
        try:
            for i in rg_client.resource_groups.list():
                list_of_resource_groups.append(process_rg_instance(i))

        except Exception as e:
            logging.error("encountered: {0}".format(str(e)))

    return json.dumps(list_of_resource_groups)


def process_resource_instance(resource):
    """
    Get the relevant pieces of information from a Resource instance.
    """

    return {
        "Name": resource.name,
        "Type": resource.type,
        "Location": resource.location,
        "Tags": resource.tags
    }


async def list_resources(credentials, subscription_id):
    """
    Get list of Resources for the Subscription ID provided.
    """
    list_of_resources = []
    with ResourceManagementClient(credentials, subscription_id) as resource_client:
        try:
            for i in resource_client.resources.list():
                list_of_resources.append(process_resource_instance(i))

        except Exception as e:
            logging.error("encountered: {0}".format(str(e)))

    return json.dumps(list_of_resources)


async def list_nsgs(credentials, subscription_id, resource_group):
    """
    Get list of Network Security Groups for the Subscription ID provided.
    """
    list_of_nsgs = []
    with NetworkManagementClient(credentials, subscription_id) as network_client:
        try:
            for i in network_client.network_security_groups.list(resource_group):
                list_of_nsgs.append(process_resource_instance(i))

        except Exception as e:
            logging.error("encountered: {0}".format(str(e)))

    return json.dumps(list_of_nsgs)


def process_rule_instance(rule):
    """
    Get the relevant pieces of information from a Security rule instance.
    """

    return {
        "Name": rule.name,
        "Access": rule.access,
        "Priority": rule.priority,
        "Direction": rule.direction,
        "Protocol": rule.protocol,
        "Source-port-range": rule.source_port_range,
        "Destination-port-range": rule.destination_port_range
    }


async def list_nsg_rules(credentials, subscription_id, resource_group):
    """
    Get list of Network Security Group Rules for the Subscription ID and Resource Group provided.
    """
    list_of_rules = []
    with NetworkManagementClient(credentials, subscription_id) as network_client:
        try:
            for i in network_client.network_security_groups.list(resource_group):
                for sec_rule in network_client.security_rules.list(resource_group_name=resource_group, network_security_group_name=i.name):
                    print(f"Security rule: {sec_rule}")
                    list_of_rules.append(process_rule_instance(sec_rule))
                
        except Exception as e:
            logging.error("encountered: {0}".format(str(e)))

    return json.dumps(list_of_rules)