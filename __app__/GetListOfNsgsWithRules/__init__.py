import logging
import os

import azure.functions as func

from azure.common.credentials import ServicePrincipalCredentials

# import resource from common.py
from __app__.shared.common import list_nsg_rules


async def main(req: func.HttpRequest) -> func.HttpResponse:
    """
    The main entry point to the function.
    """

    tenant_id = os.environ["AZURE_TENANT_ID"]
    client_id = os.environ["AZURE_CLIENT_ID"]
    client_secret = os.environ["AZURE_CLIENT_SECRET"]

    credentials = ServicePrincipalCredentials(tenant=tenant_id, client_id=client_id, secret=client_secret)

    subscription_id = os.environ.get('AZURE_SUBSCRIPTION_ID', None)
    if subscription_id:
        list_of_nsg_rules = await list_nsg_rules(credentials, subscription_id, "myResourceGroup")
        return func.HttpResponse(list_of_nsg_rules, mimetype="application/json")
    else:
        return func.HttpResponse(
             "Please configure AZURE_SUBSCRIPTION_ID variable with subscription ID value",
             status_code=400
        )