import logging
import azure.functions as func


def main(req: func.HttpRequest, todos: func.DocumentList) -> str:
    if not todos:
        logging.warning("Todo item not found")
    else:
        logging.info("Found Todo item, Message=%s",
                     todos[0]['message'])

    return 'OK'